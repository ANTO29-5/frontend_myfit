import React, { Component } from 'react';

import NavBar from '../components/NavBar/NavBar'
import BulleImage from '../components/BulleImage/BulleImage'

import { Div,Text } from "atomize";
import BulleTheme from '../components/BulleImage/BulleTheme';

import imageNature from "../assets/nature.jpg"

class HomeScreen extends Component {
    render() {
        return (
            <div>
                <NavBar/>

                <Div p={{t:"10rem",b:"2em", l:{xs:"1rem", md:"4rem"}, r:{xs:"1rem", md:"4rem"} }} d="flex" flexDir="column" align="center" textAlign="center">
                    
                    <Div p={{l:"5rem",r:"5rem"}}>
                        <Text tag="h1" textSize={{xs:"display2", md:"display3"}} textWeight="500" textColor="darck">Suivi Sportif Personnel</Text>
                        <Text tag="p" textSize={{xs:"subheader", md:"title"}} textWeight="400" textColor="light">Fini le gymboock traditionnel, j'utilise MY FIT. Un outil pour mon sport. </Text>
                    </Div>
                
                
                    <Div w={{xs:"85vw",md:"70vw"}} m={{t:"5rem"}}>
                        <BulleImage/>
                    </Div>

                    <Div w="100%" border="1px solid" borderColor="gray300"m={{t:"3rem"}} id="ScroolToFonctionnalités"></Div>
                    
                    <Div m={{t:"1rem"}}  w="100%" textAlign="left">
                        <Text tag="h3" textSize={{xs:"title", md:"display1"}} textWeight="500" textColor="darck">Pourquoi utiliser MY FIT ?</Text>
                        <Text tag="p" textSize={{xs:"subheader", md:"title"}} textWeight="400" textColor="light">
                            L'outil vous permet de garder un historique de vos entrainements. MY FIT se veut diffèrent par ça simplicité et par ça modularité. C'est simple vous faites ce que vous voulez. Créez vos programmes, ajouté y vos séances puis vos exercices.
                            <br/>
                            Encore plus loin, MY FIT vous accompagne dans le progrès. Vous pouvez classer un exercice avec un poids défini par vous-même comme entièrement maitrisé. Pour cela vous pouvez donc l'annoter avec un tag. 
                            <br/>
                            <br/>
                            Il existe 3 tags - Acquis / En cours / Non acquis.
                            <br/>
                            Quand un exercice est acquit c'est à vous d'indiquer la nouvelle charge avec laquelle vous allez poursuivre l'exercice.
                        </Text>

                        <Div d={{xs:"none",md:"flex"}} justify={{xs:"center",md:"space-around"}} m={{t:"3rem"}}>
                            <BulleTheme item="Programme" text="Programme split 3 jours" iconName="Add"/>
                            <BulleTheme item="Seance" text="Dos, Biceps et Abdos" iconName="Add"/>
                            <BulleTheme item="Exercice" text="Développé couché à la barre" iconName="Add"/>
                        </Div>
                    
                    
                    </Div>


                    <Div w="100%" border="1px solid" borderColor="gray300"m={{t:"3rem"}}></Div>
                    
                    <Div m={{t:"1rem"}}  w="100%" textAlign="left">
                        <Text tag="h3" textSize={{xs:"title", md:"display1"}} textWeight="500" textColor="darck">Pour quel sportif ?</Text>
                        <Text tag="p" textSize={{xs:"subheader", md:"title"}} textWeight="400" textColor="light">
                            MY FIT est l'œuvre d'un développeur passionné par les sports de force. Dans cette logique MY FIT est donc destiné aux amateurs de musculation, de crossfit ou d'haltérophilie.
                            <br/>
                            A noter que vous êtes libre de l'utilisation que vous en faites et donc vous pourriez l'utiliser pour un autre sport :)                         
                        </Text>

                        <Text m={{t:"3rem"}} tag="h3" textSize={{xs:"title", md:"display1"}} textWeight="500" textColor="darck">MY FIT est 100% gratuit</Text>
                       
                        <Text tag="p" textSize={{xs:"subheader", md:"title"}} textWeight="400" textColor="light">
                         Ouvert à tous et entièrement gratuit pour le plus grand plaisir de ses utilisateurs :)
                        </Text>
                    </Div>


                    <Div d="flex"flexDir={{xs:"column",md:"row"}} bg="gray100" m={{t:"4rem"}} align="center" justify="space-around" p="1rem" rounded="md" shadow="3" w="90%">

                        <Div w={{xs:"100%",md:"40%"}} rounded="md" shadow="3" order={{ xs: 2, md: 1 }} m={{xs:"1rem"}}>
                            <Div shadow="5" h="20rem" maxH="25rem" bgSize="cover" bgPos="bottom" rounded="md" bgImg={imageNature}>
                            </Div>
                        </Div>

                        <Div w={{xs:"100%",md:"50%"}} textAlign={{xs:"center",md:"left"}} order={{ xs: 1, md: 2 }}>
                            <Text tag="h3"  textSize={{xs:"title", md:"title"}} textWeight="500" textColor="darck">MY FIT est soucieux de l'environnement</Text>                    
                            <Text tag="p" textSize={{xs:"subheader", md:"subheader"}} textWeight="400" textColor="light">
                            Inquiet de son impact écologique sur notre belle planète un tas de bonnes pratiques ont été suivies et appliquées lors du développement pour avoir un impact environnemental le plus faible possible. </Text>
                        </Div>



                    </Div>

                </Div>


                 


            </div>
        );
    }
}

export default HomeScreen;