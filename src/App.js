// Routes components
import Routes from "./routes/Routes";

//Framework UI atomize
import { ThemeProvider } from 'atomize';
import "./App.css"

const theme = {
  colors: {
    nxoBleu: "#428BCA",
    nxoRouge: "red",
  }
};

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Routes />
    </ThemeProvider>
  );
}

export default App;
