import React from 'react';
import { Div } from "atomize";

const BulleImage = () => {
    return (
        <Div bg="gray100" d="flex" align="center" p="1rem" rounded="md" shadow="3">
            <Div shadow="5" w="100%" h="50vw" maxH="35rem" bgSize="cover" bgPos="center"  rounded="md" bgImg="https://images.unsplash.com/photo-1559963629-38ed0fbd4c86?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80">
            </Div>
        </Div>
    );
};

export default BulleImage;