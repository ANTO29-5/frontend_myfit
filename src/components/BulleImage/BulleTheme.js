import React from 'react';
import { Div,Icon,Text } from "atomize";

const BulleTheme = (props) => {
    return (
        <Div p={{md:"1rem",lg:"2rem"}} rounded="md" d="flex" flexDir="column" bg="#fff" shadow="3">
            <Icon name={props.iconName} size="40px" />
            <Text tag="h3" m={{t:"1rem"}} textSize={{xs:"body", md:"body"}} textWeight="600" textColor="darck">{props.item}</Text>
            <Text tag="p" textSize={{xs:"body", md:"body"}} textWeight="400" textColor="darck">{props.text}</Text>
        </Div>
    );
};

export default BulleTheme;