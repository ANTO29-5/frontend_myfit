import React, { Component } from 'react';

import { Div,Text } from "atomize";
import DrawerUtilisateurInformations from "./DrawerUtilisateurInformations"

class NavBarUser extends Component {
    render() {
        return (
            <Div bg="gray100" d="flex" align="center" justify="space-between" p="1rem" textWeight="500" textSize="2em" pos="fixed" w="100vw">
                <Div m={{ l:{ xs: '0rem', md:'0%',lg:'10%'}}}>
                    <Text textSize="subheader">MY FIT</Text>
                </Div>
                <DrawerUtilisateurInformations/>
            </Div>
        );
    }
}

export default NavBarUser;