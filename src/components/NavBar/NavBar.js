import React, { Component } from 'react';

import { Div,Text,scrollTo } from "atomize";

import DrawerConnexion from './DrawerConnexion'
import DrawerNavBar from './DrawerNavBar'


class NavBar extends Component {
    render() {
        return (
            <Div bg="gray100" d="flex" align="center" justify="space-between" p="1rem" textWeight="500" textSize="2em" pos="fixed" w="100vw">

                <Div m={{ l:{ xs: '0rem', md:'0%',lg:'10%'}}}>
                    <Text textSize="subheader">MY FIT</Text>
                </Div>
               
                <Div d={{ xs: "none", md: "flex" }} m={{ r:{ xs: '0rem',md:'0%',lg:'10%' }}} align="center">
                    <Text textColor="black700" hoverTextColor="dark"  textSize="body" onClick={() => scrollTo("#ScroolToFonctionnalités")}>Fonctionnalités</Text>
                    <a href="/test" style={{margin:"0 20px"}}><Text textColor="black700" hoverTextColor="dark"  textSize="body">Retours des utilisateurs</Text></a>
                    <DrawerConnexion/>
                </Div>

                <Div d={{ xs: "flex", md: "none" }} m={{ r:{ xs: '0rem',md:'0%',lg:'10%' }}} align="center">
                     <DrawerConnexion/>
                     <DrawerNavBar/> 
                </Div>
                

            </Div>
        );
    }
}

export default NavBar;