import React, { useRef, useState, useEffect} from "react";

import { Icon,Text } from "atomize";
import "../../styles/DrawerNavBar.css";

export default function DrawerNavBar() {


  const useDetectOutsideClick = (el, initialState) => {
    const [isActive, setIsActive] = useState(initialState);
  
    useEffect(() => {
      const onClick = e => {
        // If the active element exists and is clicked outside of
        if (el.current !== null && !el.current.contains(e.target)) {
          setIsActive(!isActive);
        }
      };
  
      // If the item is active (ie open) then listen for clicks outside
      if (isActive) {
        window.addEventListener("click", onClick);
      }
  
      return () => {
        window.removeEventListener("click", onClick);
      };
    }, [isActive, el]);
  
    return [isActive, setIsActive];
  };

  const dropdownRef = useRef(null);
  const [isActive, setIsActive] = useDetectOutsideClick(dropdownRef, false);
  const onClick = () => setIsActive(!isActive);

  return (
    <div className="container">
      <div className="menu-container">

        {!isActive ? 
        <Icon name="Menu" size="25px" m={{l:"2rem"}} onClick={onClick} />
        : 
        <Icon name="Cross" size="25px" m={{l:"2rem"}} onClick={onClick} />
      }

        <nav ref={dropdownRef} className={`menu ${isActive ? "active" : "inactive"}`}>
          <ul>
            <li>
              <a href="/test" style={{margin:"0 20px"}}><Text textColor="black700" hoverTextColor="dark" textSize="body">Fonctionnalités</Text></a>
            </li>
            <li>
              <a href="/test" style={{margin:"0 20px"}}><Text textColor="black700" hoverTextColor="dark" textSize="body">Retours des utilisateurs</Text></a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
}
