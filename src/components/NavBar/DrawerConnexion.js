// Basic Sidedrawer
import { Div, Button, SideDrawer, Icon,Text } from "atomize";
import React from 'react';
import FormulaireConnexion from "../Formulaires/FormulaireConnexion"
import FormulaireInscription from "../Formulaires/FormulaireInscription"


const BasicSideDrawer = ({ isOpen, onClose }) => {

  function useToggle(initialValue = true) {
    const [value, setValue] = React.useState(initialValue);
    const toggle = React.useCallback(() => {
      setValue(v => !v);
    }, []);
    return [value, toggle];
  }

  const [formActive, toggleFormActive] = useToggle();

  return (

    <SideDrawer isOpen={isOpen} onClose={onClose} h={{xs:"110vh",sm:"100vh"}}>


      <Div w="100%" d="flex" justify="flex-end" m={{ b: "2rem" }}>
        <Icon name="Cross" onClick={onClose} />
      </Div>

      {formActive ? 
      <>
        <Div d="flex" flexDir="column" justify="center" textAlign='center'>
            <Text tag="h3" textSize="title"> Connectez vous à votre compte. </Text>
            <Text tag="p" textSize="subheader">Vous n'avez pas de compte ?</Text>
            <Button textColor="info700" bg="#fff" onClick={toggleFormActive} ><Text textDecor="underline">Je créer mon compte.</Text></Button>
        </Div> 
        <FormulaireConnexion/>
      </>
      :
      <>
        <Div d="flex" flexDir="column" justify="center" textAlign='center'>
            <Text tag="h3" textSize="title"> Créer votre compte. </Text>
            <Text tag="p" textSize="subheader">Vous avez déjà un compte ?</Text>
            <Button textColor="info700" bg="#fff" onClick={toggleFormActive} ><Text textDecor="underline">Je me connecte.</Text></Button>
        </Div> 
        <FormulaireInscription/>
      </>
      }
      
      
    </SideDrawer>
  );
};

class DrawerConnexion extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      showSideDrawer: false
    };
  }

  render() {
    const { showSideDrawer } = this.state;

    return (
      <>
        <Button textSize="body" m={{ l: "20px" }} h={{xs:"2.5rem", md:"2.5rem"}}  bg="gray400" textColor="black700" hoverBg="gray500"  onClick={() => this.setState({ showSideDrawer: true })}>
          Connexion
         </Button>

        <BasicSideDrawer
          isOpen={showSideDrawer}
          onClose={() => this.setState({ showSideDrawer: false })}
        />

      </>
    );
  }
}

export default DrawerConnexion;