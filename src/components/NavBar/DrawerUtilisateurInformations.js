import React from 'react';

import { Div, SideDrawer, Icon,Text,Button } from "atomize";

import FormulaireUtilisateurModification from "../Formulaires/FormulaireUtilisateurModification"
import FormulaireUtilisateurPassword from "../Formulaires/FormulaireUtilisateurPassword"

const BasicSideDrawer = ({ isOpen, onClose }) => {

    function useToggle(initialValue = true) {
        const [value, setValue] = React.useState(initialValue);
        const toggle = React.useCallback(() => {
          setValue(v => !v);
        }, []);
        return [value, toggle];
      }

    const [formActive, toggleFormActive] = useToggle();

    
  return (
    <SideDrawer isOpen={isOpen} onClose={onClose} h={{xs:"110vh",sm:"100vh"}}>

      <Div w="100%" d="flex" justify="flex-end" m={{ b: "2rem" }}>
        <Icon name="Cross" onClick={onClose} />
      </Div>

      <Div d="flex" flexDir="column" justify="center" textAlign='center'>
        <Text tag="h3" textSize="title"> Mon compte utilisateur </Text>
     </Div> 

     <Div m={{t:'2rem'}} d="flex" flexDir="column" justify="center" >
        {formActive ? 
            <>
                <FormulaireUtilisateurModification/>
                <Button textColor="info700" bg="#fff" onClick={toggleFormActive} >Modifier mon mot de passe.</Button>
            </>
        : 
            <>
                <FormulaireUtilisateurPassword/>
                <Button textColor="info700" bg="#fff" onClick={toggleFormActive} >Annuler</Button>
            </>
        }
     </Div> 

    </SideDrawer>
  );
};

class DrawerUtilisateurInformations extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      showSideDrawer: false
    };
  }

  render() {
    const { showSideDrawer } = this.state;

    return (
      <>
        <Icon onClick={() => this.setState({ showSideDrawer: true })} m={{ r:{ xs: '0rem', md:'0%',lg:'10%'}}} name="UserSolid" size="20px" />

        <BasicSideDrawer
          isOpen={showSideDrawer}
          onClose={() => this.setState({ showSideDrawer: false })}
        />

      </>
    );
  }
}

export default DrawerUtilisateurInformations;