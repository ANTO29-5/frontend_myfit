import React , {useState} from 'react';

import { useForm } from 'react-hook-form';

import {Div,Input,Text,Notification,Icon} from "atomize";

import UserDataService from "../../services/user.service";


const FormulaireUtilisateurModification = () => {

    const { register, handleSubmit, errors } = useForm();

    const [successDark, setSuccessDark] = useState(false);

    const onSubmit = data =>{
        alert("nom : " + data.nom);

        /*
            UserDataService.update(data)
            .then(response => {
            //Stockage de l'utilisateur en session locale
            localStorage.setItem('userStorage', JSON.stringify(data));
            setDangerDark(current => !current)

            })
            .catch(e => {
              console.log(e);
            });
            */

    }


    return (
    <form onSubmit={handleSubmit(onSubmit)} >

        {/* Notification en cas de probleme */}
        <Notification bg="success700" isOpen={successDark} onClose={() => setSuccessDark(current => !current)}
          prefix={
            <Icon name="CloseSolid" color="white" size="1rem" m={{ r: "0.5rem" }}/>
          }
        >
         <Text tag="p" textSize="body">Votre profil utilisateur est actualisé.</Text> 
        </Notification>

        <Div m={{l:"2rem", r:"2rem"}} p={{b:"2rem"}}>

            <Div>
                <Text tag="p" textSize="body">Nom *: </Text>
                <Input type="text" placeholder="Nom" name="nom" ref={register({required: "Nom obligatoire."})} />
                {errors.nom && <Text textSize="caption" textColor="red">{errors.nom.message}</Text>}
            </Div>

            <Div m={{t:"1rem"}}>
                <Text tag="p" textSize="body">Prenom *: </Text>
                <Input type="text" placeholder="Prenom" name="prenom" ref={register({required: "Prenom obligatoire."})} />
                {errors.prenom && <Text textSize="caption" textColor="red">{errors.prenom.message}</Text>}
            </Div>

            <Div m={{t:"1rem"}}>
                <Text tag="p" textSize="body">Email *: </Text>
                <Input type="text" placeholder="Email" name="email" ref={register({required: "Email obligatoire.",pattern:{value:/^\S+@\S+$/i, message:"Format de l'email impossible"} })} />
                {errors.email && <Text textSize="caption" textColor="red">{errors.email.message}</Text>}
            </Div>

            <Input m={{t:"1.5rem"}} type="submit" value="Modifier" />

        </Div>
      </form>
    );
};

export default FormulaireUtilisateurModification;