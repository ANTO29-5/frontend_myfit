import React , {useState} from 'react';

import { useForm } from 'react-hook-form';

import {Div,Input,Text,Notification,Icon} from "atomize";

import UserDataService from "../../services/user.service";

import { useHistory } from "react-router-dom";

const FormulaireInscription = () => {

    const history = useHistory();
    const [dangerDark, setDangerDark] = useState(false);

    const { register, handleSubmit, errors } = useForm();

    const onSubmit = data =>{

        if(data.password === data.confirmationPassword){

            UserDataService.create(data)
            .then(response => {

            //Suppresion de confirmationPassword de l'objet
            delete data['confirmationPassword'];

            //Ajout de l'id utilisateur dans l'objet
            data.id = response.data.id;

            //Stockage de l'utilisateur en session locale
            localStorage.setItem('userStorage', JSON.stringify(data));
                
            //Navigation en utilisant history
            history.push("/programme");
            
            })
            .catch(e => {
              console.log(e);
            });

        }else{
            setDangerDark(current => !current)
        }
    }


    return (
    <form onSubmit={handleSubmit(onSubmit)} >

        {/* Notification en cas de mauvaise confirmation du mot de passe */}
        <Notification bg="danger700" isOpen={dangerDark} onClose={() => setDangerDark(current => !current)}
          prefix={
            <Icon name="CloseSolid" color="white" size="1rem" m={{ r: "0.5rem" }}/>
          }
        >
         <Text tag="p" textSize="body">Confirmation du mot de passe incorrect</Text> 
        </Notification>

        <Div m={{l:"2rem", r:"2rem"}} p={{b:"2rem"}}>

            <Div>
                <Text tag="p" textSize="body">Nom *: </Text>
                <Input type="text" placeholder="Nom" name="nom" ref={register({required: "Nom obligatoire."})} />
                {errors.nom && <Text textSize="caption" textColor="red">{errors.nom.message}</Text>}
            </Div>

            <Div m={{t:"1rem"}}>
                <Text tag="p" textSize="body">Prenom *: </Text>
                <Input type="text" placeholder="Prenom" name="prenom" ref={register({required: "Prenom obligatoire."})} />
                {errors.prenom && <Text textSize="caption" textColor="red">{errors.prenom.message}</Text>}
            </Div>

            <Div m={{t:"1rem"}}>
                <Text tag="p" textSize="body">Email *: </Text>
                <Input type="text" placeholder="Email" name="email" ref={register({required: "Email obligatoire.",pattern:{value:/^\S+@\S+$/i, message:"Format de l'email impossible"} })} />
                {errors.email && <Text textSize="caption" textColor="red">{errors.email.message}</Text>}
            </Div>

            <Div m={{t:"1rem"}}>
                <Text tag="p" textSize="body">Mot de passse *:</Text>
                <Input type="password" placeholder="Mot de passe" name="password" ref={register({required: true})} />
                {errors.password && <Text textSize="caption" textColor="red">Mot de passe obligatoire.</Text>}
            </Div>

            <Div m={{t:"1rem"}}>
                <Text tag="p" textSize="body">Confirmation du mot de passse *:</Text>
                <Input type="password" placeholder="Confirmation mot de passe" name="confirmationPassword" ref={register({required: true})} />
                {errors.confirmationPassword && <Text textSize="caption" textColor="red">Confirmation du mot de passe obligatoire.</Text>}
            </Div>

            <Input m={{t:"1.5rem"}} type="submit" />
        </Div>
      </form>
    );
};

export default FormulaireInscription;