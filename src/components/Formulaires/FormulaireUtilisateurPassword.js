import React , {useState} from 'react';

import { useForm } from 'react-hook-form';

import {Div,Input,Text,Notification,Icon} from "atomize";

import UserDataService from "../../services/user.service";


const FormulaireUtilisateurPassword = () => {

    const { register, handleSubmit, errors } = useForm();

    const [successDark, setSuccessDark] = useState(false);

    const onSubmit = data =>{
        alert("nom : " + data.password);

        /*
            UserDataService.update(data)
            .then(response => {
            //Stockage de l'utilisateur en session locale
            localStorage.setItem('userStorage', JSON.stringify(data));
            setDangerDark(current => !current)

            })
            .catch(e => {
              console.log(e);
            });
            */

    }


    return (
    <form onSubmit={handleSubmit(onSubmit)} >

        {/* Notification en cas de probleme */}
        <Notification bg="success700" isOpen={successDark} onClose={() => setSuccessDark(current => !current)}
          prefix={
            <Icon name="CloseSolid" color="white" size="1rem" m={{ r: "0.5rem" }}/>
          }
        >
         <Text tag="p" textSize="body">Mot de passe actualisé.</Text> 
        </Notification>

        <Div m={{l:"2rem", r:"2rem"}} p={{b:"2rem"}}>

            <Div>
                <Text tag="p" textSize="body">Mot de passe *: </Text>
                <Input type="password" placeholder="Mot de passe" name="password" ref={register({required: "Mot de passe obligatoire."})} />
                {errors.password && <Text textSize="caption" textColor="red">{errors.password.message}</Text>}
            </Div>

            <Div m={{t:"1rem"}}>
                <Text tag="p" textSize="body">Nouveau mot de passe *: </Text>
                <Input type="password" placeholder="Nouveau mot de passe" name="nouveauPassword" ref={register({required: "Nouveau mot de passe obligatoire."})} />
                {errors.nouveauPassword && <Text textSize="caption" textColor="red">{errors.nouveauPassword.message}</Text>}
            </Div>

            <Div m={{t:"1rem"}}>
                <Text tag="p" textSize="body">Confirmation *: </Text>
                <Input type="password" placeholder="Confirmation" name="confirmationNouveauPassword" ref={register({required: "Confirmation du nouveau mot de passe obligatoire."})} />
                {errors.confirmationNouveauPassword && <Text textSize="caption" textColor="red">{errors.confirmationNouveauPassword.message}</Text>}
            </Div>

            <Input m={{t:"1.5rem"}} type="submit" value="Modifier le mot de passe" />

        </Div>
      </form>
    );
};

export default FormulaireUtilisateurPassword;