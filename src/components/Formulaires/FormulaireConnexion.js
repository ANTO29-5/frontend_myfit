import React, {useState,useEffect} from 'react';

import { useForm } from 'react-hook-form';
import {Div,Input,Text,Notification,Icon,Label,Checkbox} from "atomize";

import UserDataService from "../../services/user.service";

import { useHistory } from "react-router-dom";


const FormulaireConnexion = () => {

    const history = useHistory();

    const { register, handleSubmit, errors } = useForm();
    const [dangerDark, setDangerDark] = useState(false);
    const [checkedMemory, setCheckedMemory] = useState(false);
    const [state,setState] = useState({email:'',password:'',checkedMemory:false});

    useEffect (() => { 
      //Récuperation des informations de login utilisateur dans le LocalStorage  
      if (localStorage.getItem('userStorage')) {
         let objUser =  JSON.parse(localStorage.getItem('userStorage'));
         setState({email:objUser.email,password:objUser.password})
         setCheckedMemory(objUser.checkedMemory);
      }

    }, [])


    const onSubmit = data =>{

            UserDataService.findByEmailAndPassword(data)
            .then(response => {

            if (Object.keys(response.data).length !== 0) {

                //Mise à jour des datas de l'objet avec les informations en base.
                data = (response.data[0]);

                if(!checkedMemory){
                  //suppression du password et de saveMe de l'objet user
                  delete data['password'];
                  delete data['saveMe'];
                }else{
                   //mise en memoire de l'utilisation de "Se souvenir de moi"
                  data['checkedMemory'] = true;
                }

                 //Stockage de l'utilisateur en session locale
                localStorage.setItem('userStorage', JSON.stringify(data));

                //Navigation en utilisant history
                history.push("/programme");

              }else{
                 setDangerDark(current => !current)
              }
            })
            .catch(e => {
              console.log(e);
            });

    }



    return (
    <form onSubmit={handleSubmit(onSubmit)}>

        {/* Notification en cas de connexion invalide */}
        <Notification bg="danger700" isOpen={dangerDark} onClose={() => setDangerDark(current => !current)}
          prefix={
            <Icon name="CloseSolid" color="white" size="1rem" m={{ r: "0.5rem" }}/>
          }
        >
         <Text tag="p" textSize="body">Echec de la connexion</Text> 
        </Notification>


        <Div m={{l:"2rem", r:"2rem"}}>
            <Div m={{t:"1rem"}}>
                <Text tag="p" textSize="body">Email : </Text>
                <Input type="text" placeholder="Email" name="email" defaultValue={state.email} ref={register({required: "Email obligatoire.",pattern:{value:/^\S+@\S+$/i, message:"Format de l'email impossible"} })} />
                {errors.email && <Text textSize="caption" textColor="red">{errors.email.message}</Text>}
            </Div>

            <Div m={{t:"1rem"}}>
             <Text tag="p" textSize="body">Mot de passse :</Text>
             <Input type="password" placeholder="Mot de passe" name="password" defaultValue={state.password} ref={register({required: true})} />
             {errors.password && <Text textSize="caption" textColor="red">Mot de passe obligatoire.</Text>}
            </Div>

            <Label align="center" textWeight="600" m={{t:"1rem"}}>
                <Checkbox
                    onChange={() => setCheckedMemory(!checkedMemory)}
                    checked={checkedMemory}
                />
                Se souvenir de moi
            </Label>

            <Input m={{t:"1rem"}} type="submit"  bg="#fff"/>
        </Div>
      </form>
    );
};

export default FormulaireConnexion;
