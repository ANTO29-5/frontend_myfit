import React, { Component } from 'react';

import { Switch, Route } from "react-router-dom";

import HomeScreen from "../screens/HomeScreen.js";
import ProgrammeScreen from "../screens/ProgrammeScreen.js";
import TestScreen from "../screens/TestScreen.js";



class Routes extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={HomeScreen} />
                <Route exact path="/programme" component={ProgrammeScreen} />

                <Route exact path="/test" component={TestScreen} />
            </Switch>
        );
    }
}

export default Routes;